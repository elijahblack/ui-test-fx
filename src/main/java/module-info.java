module com.example.uitestfx {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.web;
    requires Saxon.HE;

    opens com.example.uitestfx to javafx.fxml;
    exports com.example.uitestfx;
}