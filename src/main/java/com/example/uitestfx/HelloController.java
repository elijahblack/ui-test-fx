package com.example.uitestfx;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.web.WebView;
import javafx.scene.web.WebEngine;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;

import net.sf.saxon.TransformerFactoryImpl;

import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.util.Objects;
import java.util.concurrent.Flow;

public class HelloController {
    //@formatter:off
    @FXML private WebView webView;
    @FXML private TextField fileXsl;
    @FXML private TextField fileXml;
//    @FXML private TextField fileSaxon;
    @FXML private Pane paneXsl;
    @FXML private Pane paneXml;
//    @FXML private Pane paneSaxon;
    //@formatter:on


    @FXML
    protected void onRunClick() throws IOException, TransformerException {
        String xml = fileXml.getText();
        String xsl = fileXsl.getText();

        testWebView(saxonTransform(xml, xsl));
    }

    @FXML
    protected void onTestDataClick() {
        fileXsl.setText("D:\\_utils\\ultimate-xslt\\template.xsl");
        fileXml.setText("D:\\_utils\\ultimate-xslt\\document.xml");

//        fileSaxon.setText("ТУТ ПОКА ПУСТО");
//        paneSaxon.setVisible(false);
    }

    public static String saxonTransform(String source, String xslt) throws TransformerException, FileNotFoundException {
        TransformerFactoryImpl f = new net.sf.saxon.TransformerFactoryImpl();
        f.setAttribute("http://saxon.sf.net/feature/version-warning", Boolean.FALSE);
        StreamSource xsrc = new StreamSource(new FileInputStream(xslt));
        Transformer t = f.newTransformer(xsrc);
        StreamSource src = new StreamSource(new FileInputStream(source));
        StreamResult res = new StreamResult(new ByteArrayOutputStream());
        t.transform(src, res);
        return res.getOutputStream().toString();
    }

    private void testWebView(String html) {
        WebEngine webEngine = webView.getEngine();
        webEngine.loadContent(html);
    }
}